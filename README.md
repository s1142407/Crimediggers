info

1. GEO
- 2 .GPX files (gps1.gpx and gps2.gpx)
- handler contains 1 php function
- getCrossingGeoData($gpxFilename_1, $gpxFilename_2, $maxDiff_lat, $maxDiff_lon, $maxDiff_time)
- example (working for our specific case)
    $filename1 = "gps1.gpx";
    $filename2 = "gps2.gpx";
    $maxDiff_lat = 0.0001;
    $maxDiff_lon = 0.0001;
    $maxDiff_time = 1200;

    print getCrossingGeoData($filename1,$filename2, $maxDiff_lat, $maxDiff_lon, $maxDiff_time);

    OUTPUT:
        52.3605;4.8745
