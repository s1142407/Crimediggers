<?php
/**
 * This functions returns an latitude and longitude based on when the 2 .gpx files meets each other
 *
 * This function is coded by Gerard Jan Meurer <https://gitlab.com/ItsJulian> for the GEO challenge (https://www.crimediggers.nl/).
 * The GEO Challenge is an challenge created by the Dutch Cybercrime Police Department.
 * The story behind the challenge is that de police had arrested 2 suspect for a cybercrime and the want to know if the 2 suspect have met each other.
 * The only other information you got is the 2 .gpx files and you win the challenge when you submit the valid Latitude and Longitude.
 *
 * @param string $gpxFilename_1 This variable contains the filename (Full Path) to the first .gpx file
 * @param string $gpxFilename_2 This variable contains the filename (Full Path) to the second .gpx file
 * @param int $maxDiff_lat This variable contains the max allowed difference between two latitudes
 * @param int $maxDiff_lon This variable contains the max allowed difference between two longitudes
 * @param int $maxDiff_time This variable contains the max allowed difference between two timestamps
 *
 * @return string with the coordinates in the format XX.XXXX;YY.YYYY. When no coordinate is found or there is an error the string starts with 'Error:' followed with the error message
 */
function getCrossingGeoData($gpxFilename_1, $gpxFilename_2, $maxDiff_lat, $maxDiff_lon, $maxDiff_time){
    //Create 2 variables to store the XML file content
    $gpx1_input = simplexml_load_file($gpxFilename_1, 'SimpleXMLElement',LIBXML_NOWARNING);
    $gpx2_input = simplexml_load_file($gpxFilename_2, 'SimpleXMLElement',LIBXML_NOWARNING);

    //The following if statement checks if the first xml file ($gpx1_input) doesn't exists or if the file is empty
    if($gpx1_input === FALSE) {
        return "Error: The file $gpxFilename_1 doesn't exists or is empty";
    }

    //The following if statement checks if the first xml file ($gpx2_input) doesn't exists or if the file is empty
    if($gpx2_input === FALSE) {
        return "Error: The file $gpxFilename_2 doesn't exists or is empty";
    }

    //The following if statement checks if the given input $maxDiff_lat is not a number
    if(!is_numeric($maxDiff_lat)) {
        return "Error: The variable '$maxDiff_lat' is not a number";
    }

    //The following if statement checks if the given input $maxDiff_lon is not a number
    if(!is_numeric($maxDiff_lon)) {
        return "Error: The variable '$maxDiff_lon' is not a number";
    }

    //The following if statement checks if the given input $maxDiff_time is not a number
    if(!is_numeric($maxDiff_time)) {
        return "Error: The variable '$maxDiff_time' is not a number";
    }

    //We are creating 2 empty arrays so that further on we can push the arrays with the data we want to the initializes arrays
    $gpx1_parsed = array();
    $gpx2_parsed = array();

    //The following foreach statement create an array with the keys 'lat'. 'lon', 'time'.
    //After creating the array and setting the values to the latitude, longitude and time
    //When the array is created it pushes the array we have just created to the $gpx1_parsed array
    foreach ($gpx1_input->wpt as $gps1) {
        $gps = array("lat"=> (string) $gps1['lat'], "lon"=> (string) $gps1['lon'], "time"=>strtotime((string) $gps1->time));
        array_push($gpx1_parsed, $gps);
    }

    //The following foreach statement create an array with the keys 'lat'. 'lon', 'time'.
    //After creating the array and setting the values to the latitude, longitude and time
    //When the array is created it pushes the array we have just created to the $gpx2_parsed array
    foreach ($gpx2_input->wpt as $gps2) {
        $gps = array("lat"=> (string) $gps2['lat'], "lon"=> (string) $gps2['lon'], "time"=>strtotime((string) $gps2->time));
        array_push($gpx2_parsed, $gps);
    }

    //We loop through the following statements until the variable $output is set
    while (!isset($output)){
        //We loop through all the data from the $gpx1_input file pushed to the $gpx1_parsed array
        foreach ($gpx1_parsed as $gpx1){
            //For each array in $gpx1_parsed we will loop through all the arrays in the $gpx2_parsed array
            foreach ($gpx2_parsed as $gpx2){
                //We created a variable called $latitudeDifference and store the difference between the latitude from $gpx1 and $gpx2
                $latitudeDifference = abs($gpx1['lat'] - $gpx2['lat']);
                //We created a variable called $longitudeDifference and store the difference between the longitude from $gpx1 and $gpx2
                $longitudeDifference = abs($gpx1['lon'] - $gpx2['lon']);
                //We created a variable called $timeDifference and store the difference between the time from $gpx1 and $gpx2
                $timeDifference = abs($gpx1['time'] - $gpx2['time']);

                //We check if the $latitudeDifference is smaller than the given input $maxDiff_lat.
                //And if $longitudeDifference is smaller than the given input $maxDiff_lon.
                //And if $timeDifference is smaller than the given input $maxDiff_time
                if($latitudeDifference < $maxDiff_lat && $longitudeDifference < $maxDiff_lon && $timeDifference < $maxDiff_time){
                    //if all the 3 conditions above are true we will set the output variable with the latitude rounded to 4 decimal.
                    //After the latitude we will place a ';'
                    //And after that we will place the longitude rounded to 4 decimal
                    $output = round($gpx1['lat'], 4) . ";".round($gpx1['lon'], 4);
                    //After setting the output we stop the foreach loop
                    break;
                }
            }
        }
        //When we have looped through all data in the $gpx1_parsed array and the $output variable is stil not set we will set $output to 'Not Found'
        if(!isset($output)){
            $output = "Error: Not Found";
        }
    }
    return $output;
}

